﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using GoogleMobileAds;
using GoogleMobileAds.Api;

public class GoogleMobileAdsHandler : MonoBehaviour {

	public static GoogleMobileAdsHandler Instance;

	#region Development Build Variable

	public bool enableDevelopmentBuild;
	public bool enableTestGUI;

	public string testDeviceID;

	private const string testAppIdForBannerAd 						= "ca-app-pub-3940256099942544/6300978111";
	private const string testAppIdForInterstetialAd 				= "ca-app-pub-3940256099942544/1033173712";
	private const string testAppIdForRewardBasedVideoAd_Android 	= "ca-app-pub-3940256099942544/5224354917";
	private const string testAppIdForRewardBasedVideoAd_iOS 		= "ca-app-pub-3940256099942544/1712485313";

	#endregion

	#region Public Variables

	public bool enable_Android_Platform;
	public bool enable_iOS_Platform;

	[Header("RewardVideoAd Property")]
	public string appIdRewardBasedVideoAds_Android;
	public string appIdRewardBasedVideoAds_iOS;

	public 	bool 		enableRewardBasedVideoAd;
	public 	bool 		rewardedVideoAdSkipCallback;
	public	bool 		rewardedVideoAdFailedCallBack;

	public UnityEvent	OnRewardBasedVideoAdFinishedEvent;
	public UnityEvent	OnRewardBasedVideoAdSkippedEvent;
	public UnityEvent	OnRewardBasedVideoAdFailedEvent;

	[Header("InterstetialAd Property")]
	public string adUnitIdInterstetialAd_Android;
	public string adUnitIdInterstetialAd_iOS;

	public 	bool 		enableInterstetialAd;
	public 	bool 		interstitialAdFailedCallback;
	public 	bool 		interstitialAdClosedCallback;

	public UnityEvent	OnInterstitialAdFailedEvent;
	public UnityEvent	OnInterstitialAdClosedEvent;

	[Header("BannerAd Property")]
	public string adUnitIdBannerViewAd_Android;
	public string adUnitIdBannerViewAd_iOS;
	public AdPosition bannerAdPosition;

	public 	bool 		enableBannerViewAd;
	public 	bool 		bannerViewAdFailedCallback;
	public 	bool 		bannerViewAdClosedCallback;

	public UnityEvent	OnBannerViewAdFailedEvent;
	public UnityEvent	OnBannerViewAdClosedEvent;

	#endregion

	#region Private Variables

	private	bool mCustomCall;

	private int 	mAttemptToRetryAfterFailToLoad_RbvAd 	= 0;
	private int 	mAttemptToRetryAfterFailToLoad_IntAd 	= 0;
	private float 	mDelay = 3.0f;
	private int 	mMaxNumberOfTry = 3;

	private string adUnitIdBannerViewAd;
	private string adUnitIdInterstetialAd;
	private string appIdRewardBasedVideoAd;

	private BannerView 			mBannerView;
	private InterstitialAd 		mInterstitialAd; 
	public 	RewardBasedVideoAd 	mRewardBasedVideoAd;

	private UnityAction OnAdsFinished;
	private UnityAction OnAdsSkipped;
	private UnityAction OnAdsFailed;

	#endregion

	void Awake(){

		if (Instance == null) {

			Instance = this;
		} else if(Instance != this){

			Destroy (this.gameObject);
		}

	}

	void Start(){

		mPreProcess ();
	}

	//--------------------

	#region Public Calls

	public bool IsRewardBasedVideoAdReady(){
	
		return mRewardBasedVideoAd.IsLoaded ();
	}

	public bool IsInterstetialAdReady(){

		return mInterstitialAd.IsLoaded ();
	}

	public void ShowAds(){

		if (mRewardBasedVideoAd.IsLoaded ()) {

			mRewardBasedVideoAd.Show ();
		} else if (mInterstitialAd.IsLoaded ()) {

			mInterstitialAd.Show ();
		}
	}
		
	#endregion

	//--------------------

	#region RewardBasedVideoAd

	private void mInitializeRewardBasedVideoAd(){

		/*
		 * As RewardBasedVideoAd is a singleton class, we wonly need to get the instance once to load advertisement
		 * and event handler. Where things are different for Interstetial and banner ads
		*/

		mRewardBasedVideoAd = RewardBasedVideoAd.Instance;

		mRewardBasedVideoAd.OnAdLoaded 				+= OnRewardBasedVideoAdLoaded;
		mRewardBasedVideoAd.OnAdFailedToLoad		+= OnRewardBasedVideoAdFailedLoaded;
		mRewardBasedVideoAd.OnAdOpening 			+= OnRewardBasedVideoAdOpening;
		mRewardBasedVideoAd.OnAdStarted 			+= OnRewardBasedVideoAdStarted;
		mRewardBasedVideoAd.OnAdRewarded 			+= OnRewardBasedVideoAdRewarded;
		mRewardBasedVideoAd.OnAdClosed 				+= OnRewardBasedVideoAdClosed;
		mRewardBasedVideoAd.OnAdLeavingApplication 	+= OnRewardBasedVideoAdLeavingApplication;

		mRewardBasedVideoAd.LoadAd (mGetAdRequest(), appIdRewardBasedVideoAd);
	}

	public void ShowRewardBasedVideoAd(){

		if (mRewardBasedVideoAd.IsLoaded ()) {

			mRewardBasedVideoAd.Show ();
		} else {
		
			StartCoroutine (RequestRewardBasedVideoAd (true));
		}
	}

	public void ShowRewardBasedVideoAdWithEvent(UnityAction OnAdsFinished){

		mResetCustomAdsCallBack ();

		this.OnAdsFinished = OnAdsFinished;
		mCustomCall = true;

		ShowRewardBasedVideoAd ();
	}

	public void ShowRewardBasedVideoAdWithEvent(UnityAction OnAdsFinished,UnityAction OnAdsFailed){

		mResetCustomAdsCallBack ();

		this.OnAdsFinished = OnAdsFinished;
		this.OnAdsFailed = OnAdsFailed;
		mCustomCall = true;

		ShowRewardBasedVideoAd ();
	}

	public void ShowRewardBasedVideoAdWithEvent(UnityAction OnAdsFinished, UnityAction OnAdsFailed, UnityAction OnAdsSkipped){

		mResetCustomAdsCallBack ();

		this.OnAdsFinished = OnAdsFinished;
		this.OnAdsFailed = OnAdsFailed;
		this.OnAdsSkipped = OnAdsSkipped;
		mCustomCall = true;

		ShowRewardBasedVideoAd ();
	}

	private IEnumerator RequestRewardBasedVideoAd(bool mCallToShowAd){
	
		bool mSearch  = true;
		int  mAttempt = 0;

		while (mSearch) {

			if (!mRewardBasedVideoAd.IsLoaded ()) {
				//If : Ads is not loaded

				AdRequest mRequest = new AdRequest.Builder ().Build ();
				mRewardBasedVideoAd.LoadAd (mRequest, appIdRewardBasedVideoAd);

				//Wait for few secs to load ads
				yield return new WaitForSeconds (mDelay);
				
				mAttempt++;
				if (mAttempt == mMaxNumberOfTry) {
					// if number of attempt to load ads reach it maximum number of try

					mSearch = false;
					if (mRewardBasedVideoAd.IsLoaded () && mCallToShowAd) {
						// if : it was requested to show ads, not only to load ads
						mRewardBasedVideoAd.Show ();
					}
				}
			} else {
				// if : the ads is loaded before reaching its maximum limit of attempt
				mSearch = false;
				if (mCallToShowAd) {
					// if : it was requested to show ads, not only to load ads
					mRewardBasedVideoAd.Show ();
				}
			}
		}
	}

	private void OnRewardBasedVideoAdLoaded(object sender,EventArgs args){}

	private void OnRewardBasedVideoAdFailedLoaded(object sender,AdFailedToLoadEventArgs args){

		//Try to reload ads with saftely measure
		if (mAttemptToRetryAfterFailToLoad_RbvAd >= mMaxNumberOfTry) {

			mAttemptToRetryAfterFailToLoad_RbvAd = 0;
		} else {

			StartCoroutine (RequestRewardBasedVideoAd (false));
			mAttemptToRetryAfterFailToLoad_RbvAd++;
		}

		// If It's call from the script or preset
		if (mCustomCall) {
		
			OnAdsFailed.Invoke ();
		} else {

			mCustomCall = false;
			OnRewardBasedVideoAdFailedEvent.Invoke ();
		}
	}

	private void OnRewardBasedVideoAdOpening(object sender,EventArgs args){}

	private void OnRewardBasedVideoAdStarted(object sender,EventArgs args){}

	private void OnRewardBasedVideoAdClosed(object sender,EventArgs args){

		// If It's call from the script or preset
		if (mCustomCall) {

			OnAdsSkipped.Invoke ();
		} else {

			mCustomCall = false;
			OnRewardBasedVideoAdSkippedEvent.Invoke ();
		}
	}

	private void OnRewardBasedVideoAdRewarded(object sender,EventArgs args){

		// If It's call from the script or preset
		if (mCustomCall) {

			OnAdsFinished.Invoke ();
		} else {

			mCustomCall = false;
			OnRewardBasedVideoAdFinishedEvent.Invoke ();
		}
	}

	private void OnRewardBasedVideoAdLeavingApplication(object sender,EventArgs args){}

	#endregion

	//--------------------

	#region InterstetialAd

	public void ShowInterstetialAd(){

		if (mInterstitialAd.IsLoaded ()) {

			mInterstitialAd.Show ();
		} else {

			StartCoroutine (RequestInterstitialAd (true));
		}
	}

	public void ShowInterstetialAd(UnityAction OnAdsFailed){

		mResetCustomAdsCallBack ();

		this.OnAdsFailed = OnAdsFailed;
		mCustomCall = true;

		ShowInterstetialAd ();
	}

	public void ShowInterstetialAd(UnityAction OnAdsFailed, UnityAction OnAdsClosed){

		mResetCustomAdsCallBack ();

		this.OnAdsFailed = OnAdsFailed;
		this.OnAdsSkipped = OnAdsClosed;
		mCustomCall = true;

		ShowInterstetialAd ();
	}

	private IEnumerator RequestInterstitialAd(bool mCallToShowAd){

		bool mSearch  = true;
		int  mAttempt = 0;

		while (mSearch) {

			if (!mInterstitialAd.IsLoaded ()) {
				//If : Ads is not loaded

				mInterstitialAd = new InterstitialAd (adUnitIdInterstetialAd);

				mInterstitialAd.OnAdLoaded 				+= OnInterstitialAdLoaded;
				mInterstitialAd.OnAdFailedToLoad 		+= OnInterstitialAdFailedLoaded;
				mInterstitialAd.OnAdOpening 			+= OnInterstitialAdOpened;
				mInterstitialAd.OnAdClosed 				+= OnInterstitialAdClosed;
				mInterstitialAd.OnAdLeavingApplication 	+= OnInterstitialAdLeavingApplication;

				mInterstitialAd.LoadAd (mGetAdRequest());

				yield return new WaitForSeconds (mDelay);

				mAttempt++;
				if (mAttempt == mMaxNumberOfTry) {

					mSearch = false;
					if (mInterstitialAd.IsLoaded () && mCallToShowAd)
						mInterstitialAd.Show ();
				}
			} else {

				mSearch = false;
				if (mCallToShowAd)
					mInterstitialAd.Show ();
			}
		}
	}

	private void OnInterstitialAdLoaded(object sender,EventArgs args){}

	private void OnInterstitialAdFailedLoaded(object sender,AdFailedToLoadEventArgs args){

		//Try to reload ads with saftely measure
		if (mAttemptToRetryAfterFailToLoad_IntAd == mMaxNumberOfTry) {

			mAttemptToRetryAfterFailToLoad_IntAd = 0;
		} else {

			StartCoroutine (RequestInterstitialAd (false));
			mAttemptToRetryAfterFailToLoad_IntAd++;
		}

		// If It's call from the script or preset
		if (mCustomCall) {

			OnAdsFailed.Invoke ();
		} else {

			mCustomCall = false;
			OnInterstitialAdFailedEvent.Invoke ();
		}

	}

	private void OnInterstitialAdOpened(object sender,EventArgs args){}

	private void OnInterstitialAdClosed(object sender,EventArgs args){

		// If It's call from the script or preset
		if (mCustomCall) {

			OnAdsSkipped.Invoke ();
		} else {

			mCustomCall = false;
			OnInterstitialAdClosedEvent.Invoke ();
		}
	}

	private void OnInterstitialAdLeavingApplication(object sender,EventArgs args){}

	#endregion

	//--------------------

	#region BannerView

	public void ShowBannerViewAd(){
	
		mBannerView = new BannerView (adUnitIdBannerViewAd, AdSize.Banner, bannerAdPosition);

		mBannerView.OnAdLoaded 				+= OnBannerViewAdLoaded;
		mBannerView.OnAdFailedToLoad		+= OnBannerViewAdFailedLoaded;
		mBannerView.OnAdOpening 			+= OnBannerViewAdOpened;
		mBannerView.OnAdClosed 				+= OnBannerViewAdClosed;
		mBannerView.OnAdLeavingApplication 	+= OnBannerViewAdLeavingApplication;

		mBannerView.LoadAd (mGetAdRequest ());
		mBannerView.Show ();
	}

	public void ShowBannerViewAd(UnityAction OnAdsFailed){
	
		mResetCustomAdsCallBack ();

		this.OnAdsFailed = OnAdsFailed;
		mCustomCall = true;

		ShowBannerViewAd ();
	}

	public void ShowBannerViewAd(UnityAction OnAdsFailed, UnityAction OnAdsClosed){

		mResetCustomAdsCallBack ();

		this.OnAdsFailed = OnAdsFailed;
		this.OnAdsSkipped = OnAdsClosed;
		mCustomCall = true;

		ShowBannerViewAd ();
	}

	public void DisplayBannerViewAd(){
	
		if (mBannerView != null) {
		
			mBannerView.Show ();
		}
	}

	public void HideBannerViewAd(){

		mBannerView.Hide ();

	}

	public void DestroyBannerViewAd(){

		mBannerView.Destroy ();
	}

	private void OnBannerViewAdLoaded(object sender,EventArgs args){}

	private void OnBannerViewAdFailedLoaded(object sender,AdFailedToLoadEventArgs args){

		// If It's call from the script or preset
		if (mCustomCall) {

			OnAdsFailed.Invoke ();
		} else {

			mCustomCall = false;
			OnBannerViewAdFailedEvent.Invoke ();
		}

	}

	private void OnBannerViewAdOpened(object sender,EventArgs args){}

	private void OnBannerViewAdClosed(object sender,EventArgs args){

		// If It's call from the script or preset
		if (mCustomCall) {

			OnAdsSkipped.Invoke ();
		} else {

			mCustomCall = false;
			OnBannerViewAdClosedEvent.Invoke ();
		}
	}

	private void OnBannerViewAdLeavingApplication(object sender,EventArgs args){}


	#endregion

	//--------------------

	#region Class Configuration

	private void mPreProcess(){

		mCustomCall = false;;

		mResetCustomAdsCallBack ();

		#region adUnitId/appId Configuretion

		if(enableDevelopmentBuild){

			adUnitIdBannerViewAd = testAppIdForBannerAd;
			adUnitIdInterstetialAd = testAppIdForInterstetialAd;

			#if UNITY_ANDROID
			appIdRewardBasedVideoAd = testAppIdForRewardBasedVideoAd_Android;
			#elif UNITY_IOS
			appUnitIdRewardBasedVideoAd = testAppIdForRewardBasedVideoAd_iOS;
			#endif

		}else{

			#if UNITY_ANDROID

			adUnitIdBannerViewAd = adUnitIdBannerViewAd_Android;
			adUnitIdInterstetialAd = adUnitIdInterstetialAd_Android;
			appIdRewardBasedVideoAd = appIdRewardBasedVideoAds_Android;

			#elif UNITY_IOS

			adUnitIdBannerAd = adUnitIdBannerAd_iOS;
			adUnitIdInterstetialAd = adUnitIdInterstetialAd_iOS;
			appIdRewardBasedVideoAd = appIdRewardBasedVideoAds_iOS;

			#endif
		}

		#endregion

		if (enableRewardBasedVideoAd) {

			mInitializeRewardBasedVideoAd ();
		}

		if (enableInterstetialAd) {
		
			mInterstitialAd = new InterstitialAd (adUnitIdInterstetialAd);
		}

		if (enableBannerViewAd) {
		
			mBannerView = new BannerView (adUnitIdInterstetialAd, AdSize.Banner, bannerAdPosition);
		}
	}

	void OnGUI(){

		if (enableTestGUI) {
		
			GUIStyle mGUILableStyle = new GUIStyle ();
			mGUILableStyle.alignment = TextAnchor.MiddleCenter;
			mGUILableStyle.fontSize = (int)(Screen.height * 0.02f);
			mGUILableStyle.normal.textColor = new Color (1.0f, 1.0f, 1.0f, 1.0f);
			mGUILableStyle.wordWrap = true;

			float mWidth = 0; 
			float mHeight = 0;

			if (Screen.width > Screen.height) {
				//Landscape
				mWidth = Screen.width/3;
				mHeight = Screen.height / 16;

			} else {
				//Portrait
				mWidth = Screen.width / 2;
				mHeight = Screen.height / 12;
			}

			#region RewardBasedVideoAd

			if(enableDevelopmentBuild){

				GUI.Label (
					new Rect (0.0f, 0.0f, Screen.width, mHeight*1.0f), 
					"RewardVideoAd (Test)\n" + appIdRewardBasedVideoAd,
					mGUILableStyle);
			}else{

				GUI.Label (
					new Rect (0.0f, 0.0f, Screen.width, mHeight*1.0f), 
					"RewardVideoAd : " + appIdRewardBasedVideoAd,
					mGUILableStyle);
			}

			if (mRewardBasedVideoAd != null) {
			
				GUI.Label (
					new Rect (0.0f, mHeight, Screen.width / 3, mHeight), 
					IsRewardBasedVideoAdReady () ? "RewardVideoAd\nReady" : "RewardVideoAd\nNot Ready",
					mGUILableStyle);
			} else {

				GUI.Label (
					new Rect (0.0f, mHeight, Screen.width / 3, mHeight), 
					"RewardVideoAd\nNot Initialized",
					mGUILableStyle);
			}

			if(GUI.Button(new Rect(Screen.width / 3.0f, mHeight, Screen.width / 3, mHeight),"Request\nRewardVideoAd")){

				StartCoroutine(RequestRewardBasedVideoAd(false));
			}

			if(GUI.Button(new Rect((Screen.width * 2.0f) / 3.0f, mHeight, Screen.width / 3, mHeight),"Show\nRewardVideoAd")){

				ShowRewardBasedVideoAd ();
			}

			#endregion

			#region InterstetialAd

			if(enableDevelopmentBuild){

				GUI.Label (
					new Rect (0.0f, mHeight*2.0f, Screen.width, mHeight), 
					"InterstetialAd (Test)\n" + adUnitIdInterstetialAd,
					mGUILableStyle);
			}else{

				GUI.Label (
					new Rect (0.0f, mHeight*2.0f, Screen.width, mHeight), 
					"InterstetialAd : " + adUnitIdInterstetialAd,
					mGUILableStyle);
			}


			if (mInterstitialAd != null) {

				GUI.Label (
					new Rect (0.0f, mHeight*3.0f, Screen.width / 3, mHeight), 
					IsInterstetialAdReady () ? "InterstetialAd\nReady" : "InterstetialAd\nNot Ready",
					mGUILableStyle);
			} else {
			
				GUI.Label (
					new Rect (0.0f, mHeight*3.0f, Screen.width / 3, mHeight), 
					"InterstetialAd\nNot Initialized",
					mGUILableStyle);
			}

			if(GUI.Button(new Rect(Screen.width / 3.0f, mHeight*3.0f, Screen.width / 3, mHeight),"Request\nInterstetialAd")){

				StartCoroutine(RequestInterstitialAd(false));
			}

			if(GUI.Button(new Rect((Screen.width * 2.0f) / 3.0f, mHeight*3.0f, Screen.width / 3, mHeight),"Show\nInterstetialAd")){

				ShowInterstetialAd ();
			}

			#endregion

			#region BannerViewAd

			if(enableDevelopmentBuild){

				GUI.Label (
					new Rect (0.0f, mHeight*4.0f, Screen.width, mHeight), 
					"BannerViewAd (Test)\n" + adUnitIdBannerViewAd,
					mGUILableStyle);
			}else{

				GUI.Label (
					new Rect (0.0f, mHeight*4.0f, Screen.width, mHeight), 
					"BannerViewAd : " + adUnitIdBannerViewAd,
					mGUILableStyle);
			}

			if(GUI.Button(new Rect(0.0f, mHeight*5.0f, Screen.width / 3, mHeight),"Show\nBannerViewAd")){

				ShowBannerViewAd();
			}

			if(GUI.Button(new Rect(Screen.width / 3.0f, mHeight*5.0f, Screen.width / 3, mHeight),"Hide\nBannerViewAd")){

				HideBannerViewAd();
			}


			if(GUI.Button(new Rect((Screen.width * 2.0f) / 3.0f, mHeight*5.0f, Screen.width / 3, mHeight),"Destroy\nBannerViewAd")){

				DestroyBannerViewAd();
			}

			#endregion
		}
	}

	private void mResetCustomAdsCallBack(){

		OnAdsFinished 	= null;
		OnAdsSkipped 	= null;
		OnAdsFailed 	= null;
	}

	private AdRequest mGetAdRequest(){

		if (enableDevelopmentBuild) {
		
			return new AdRequest.Builder ()
				.AddTestDevice (AdRequest.TestDeviceSimulator)
				.AddTestDevice (testDeviceID)
				.AddKeyword ("game")
				.Build ();

		} else {

			return new AdRequest.Builder ().Build ();
		}
	}

	#endregion
}
