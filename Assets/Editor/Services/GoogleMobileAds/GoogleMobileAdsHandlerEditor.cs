﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(GoogleMobileAdsHandler))]
public class GoogleMobileAdsHandlerEditor : Editor {

	private GoogleMobileAdsHandler mGMAHReference;

	private void mPreProcess(){

		mGMAHReference = (GoogleMobileAdsHandler)target;
	}

	private void OnEnabled(){

		mPreProcess ();
	}

	public override void OnInspectorGUI(){

		mPreProcess ();

		serializedObject.Update ();

		GUILayout.Box ("", GUILayout.ExpandWidth (true), GUILayout.Height (2.5f));

		mInstructionGUI ();

		GUILayout.Box ("", GUILayout.ExpandWidth (true), GUILayout.Height (2.5f));

		mPlatformSelectionGUI ();

		GUILayout.Box ("", GUILayout.ExpandWidth (true), GUILayout.Height (2.5f));

		if (mGMAHReference.enable_Android_Platform || mGMAHReference.enable_iOS_Platform) {

			EditorGUI.indentLevel += 1;

			mRewardBasedVideoAdGUI ();

			GUILayout.Box ("", GUILayout.ExpandWidth (true), GUILayout.Height (2.5f));

			mInterstetialAdGUI ();

			GUILayout.Box ("", GUILayout.ExpandWidth (true), GUILayout.Height (2.5f));

			mBannerViewAdGUI ();

			EditorGUI.indentLevel -= 1;
		}

		serializedObject.ApplyModifiedProperties ();
	}

	private void mInstructionGUI(){

		if (mGMAHReference.enableDevelopmentBuild) {

			EditorGUILayout.BeginHorizontal();

			EditorGUILayout.BeginVertical();
			EditorGUILayout.HelpBox ("The application will now use test appId/adUnitId given by the admob." +
				"\nYou must put your device id on the 'DeviceID' section,This will help you to get your device id." +
				"\n\nhttps://play.google.com/store/apps/details?id=com.evozi.deviceid", MessageType.Info);
			mGMAHReference.enableDevelopmentBuild = EditorGUILayout.Toggle (
				"Development Build",
				mGMAHReference.enableDevelopmentBuild);
			EditorGUILayout.EndVertical();

			if (mGMAHReference.enableTestGUI) {

				EditorGUILayout.BeginVertical();
				EditorGUILayout.HelpBox ("You will now be able to test 'AdMob' on physical devices with the help of 'OnGUI' interface", MessageType.Info);
				mGMAHReference.enableTestGUI = EditorGUILayout.Toggle (
					"Test GUI",
					mGMAHReference.enableTestGUI);
				EditorGUILayout.EndVertical();
			} else {
			
				EditorGUILayout.BeginVertical();
				EditorGUILayout.HelpBox ("'Test GUI' will allow you to test 'AdMob' on physical device by enabling 'OnGUI' interface", MessageType.Info);
				mGMAHReference.enableTestGUI = EditorGUILayout.Toggle (
					"Test GUI",
					mGMAHReference.enableTestGUI);
				EditorGUILayout.EndVertical();
			}

			EditorGUILayout.EndHorizontal();

		} else {

			EditorGUILayout.BeginHorizontal ();

			EditorGUILayout.BeginVertical();
			EditorGUILayout.HelpBox ("If your application is not linked with the playstore & appstore, click on the enable 'Development Build'", MessageType.Info);
			mGMAHReference.enableDevelopmentBuild = EditorGUILayout.Toggle (
				"Development Build",
				mGMAHReference.enableDevelopmentBuild);
			EditorGUILayout.EndVertical ();

			if (mGMAHReference.enableTestGUI) {

				EditorGUILayout.BeginVertical();
				EditorGUILayout.HelpBox ("You will now be able to test 'AdMob' on physical devices with the help of 'OnGUI' interface", MessageType.Info);
				mGMAHReference.enableTestGUI = EditorGUILayout.Toggle (
					"Test GUI",
					mGMAHReference.enableTestGUI);
				EditorGUILayout.EndVertical();
			} else {

				EditorGUILayout.BeginVertical();
				EditorGUILayout.HelpBox ("'Test GUI' will allow you to test 'AdMob' on physical device by enabling 'OnGUI' interface", MessageType.Info);
				mGMAHReference.enableTestGUI = EditorGUILayout.Toggle (
					"Test GUI",
					mGMAHReference.enableTestGUI);
				EditorGUILayout.EndVertical();
			}

			EditorGUILayout.EndHorizontal ();
		}

		if (mGMAHReference.enableDevelopmentBuild) {
		
			mGMAHReference.testDeviceID = EditorGUILayout.TextField (
				"DeviceID : ",
				mGMAHReference.testDeviceID);
		}
	}

	private void mPlatformSelectionGUI(){

		EditorGUILayout.BeginHorizontal ();
		mGMAHReference.enable_Android_Platform = EditorGUILayout.Toggle (
			"Android",
			mGMAHReference.enable_Android_Platform);
		mGMAHReference.enable_iOS_Platform = EditorGUILayout.Toggle (
			"iOS",
			mGMAHReference.enable_iOS_Platform);
		EditorGUILayout.EndHorizontal ();
	}

	private void mRewardBasedVideoAdGUI(){

		EditorGUILayout.Space ();
		mGMAHReference.enableRewardBasedVideoAd = EditorGUILayout.Foldout (
			mGMAHReference.enableRewardBasedVideoAd,
			mGMAHReference.enableRewardBasedVideoAd ? "(Enable):RewardBasedVideoAd" : "(Disabled):RewardBasedVideoAd");

		if (mGMAHReference.enableRewardBasedVideoAd) {

			EditorGUI.indentLevel += 1;

			EditorGUILayout.Space ();

			EditorGUILayout.BeginVertical ();
			if (!mGMAHReference.enableDevelopmentBuild && mGMAHReference.enable_Android_Platform) {
			
				mGMAHReference.appIdRewardBasedVideoAds_Android = EditorGUILayout.TextField (
					"appId_Android",
					mGMAHReference.appIdRewardBasedVideoAds_Android);
			}
			if(!mGMAHReference.enableDevelopmentBuild && mGMAHReference.enable_iOS_Platform){

				mGMAHReference.appIdRewardBasedVideoAds_iOS = EditorGUILayout.TextField (
					"appId_iOS",
					mGMAHReference.appIdRewardBasedVideoAds_iOS);
			}
			EditorGUILayout.EndVertical ();

			EditorGUILayout.Space ();
			EditorGUILayout.PropertyField (
				serializedObject.FindProperty ("OnRewardBasedVideoAdFinishedEvent"));

			//Skip Option
			EditorGUILayout.BeginHorizontal ();
			mGMAHReference.rewardedVideoAdFailedCallBack = EditorGUILayout.Toggle (
				"OnRewardBasedVideoAdFailed",
				mGMAHReference.rewardedVideoAdFailedCallBack);

			if (mGMAHReference.rewardedVideoAdFailedCallBack) {

				EditorGUILayout.PropertyField (
					serializedObject.FindProperty ("OnRewardBasedVideoAdFailedEvent"));
				
			}
			EditorGUILayout.EndHorizontal ();

			EditorGUILayout.BeginHorizontal ();
			mGMAHReference.rewardedVideoAdSkipCallback = EditorGUILayout.Toggle (
				"OnRewardBasedVideoAdSkip",
				mGMAHReference.rewardedVideoAdSkipCallback);

			if (mGMAHReference.rewardedVideoAdSkipCallback) {

				EditorGUILayout.PropertyField (
					serializedObject.FindProperty ("OnRewardBasedVideoAdSkippedEvent"));
			}
			EditorGUILayout.EndHorizontal ();

			EditorGUI.indentLevel -= 1;
		}
	}

	private void mInterstetialAdGUI(){

		mGMAHReference.enableInterstetialAd = EditorGUILayout.Foldout (
			mGMAHReference.enableInterstetialAd,
			mGMAHReference.enableInterstetialAd ? "(Enable):InterstitialAd" : "(Disabled):InterstitialAd");

		if (mGMAHReference.enableInterstetialAd) {
		
			EditorGUI.indentLevel += 1;

			EditorGUILayout.Space ();

			EditorGUILayout.BeginVertical ();
			if (!mGMAHReference.enableDevelopmentBuild && mGMAHReference.enable_Android_Platform) {

				mGMAHReference.adUnitIdInterstetialAd_Android = EditorGUILayout.TextField (
					"adUnitId_Android",
					mGMAHReference.adUnitIdInterstetialAd_Android);
			}
			if(!mGMAHReference.enableDevelopmentBuild && mGMAHReference.enable_iOS_Platform){

				mGMAHReference.adUnitIdInterstetialAd_iOS = EditorGUILayout.TextField (
					"adUnitId_iOS",
					mGMAHReference.adUnitIdInterstetialAd_iOS);
			}
			EditorGUILayout.EndVertical ();

			EditorGUILayout.BeginHorizontal ();
			mGMAHReference.interstitialAdFailedCallback = EditorGUILayout.Toggle (
				"OnInterstitialAdFailed",
				mGMAHReference.interstitialAdFailedCallback);

			if (mGMAHReference.interstitialAdFailedCallback) {

				EditorGUILayout.PropertyField (
					serializedObject.FindProperty ("OnInterstitialAdFailedEvent"));

			}
			EditorGUILayout.EndHorizontal ();


			EditorGUILayout.BeginHorizontal ();
			mGMAHReference.interstitialAdClosedCallback = EditorGUILayout.Toggle (
				"OnInterstitialAdClosed",
				mGMAHReference.interstitialAdClosedCallback);

			if (mGMAHReference.interstitialAdClosedCallback) {

				EditorGUILayout.PropertyField (
					serializedObject.FindProperty ("OnInterstitialAdClosedEvent"));

			}
			EditorGUILayout.EndHorizontal ();

			EditorGUI.indentLevel -= 1;
		}
	}

	private void mBannerViewAdGUI(){
	
		mGMAHReference.enableBannerViewAd = EditorGUILayout.Foldout (
			mGMAHReference.enableBannerViewAd,
			mGMAHReference.enableBannerViewAd ? "(Enable):BannerViewAd" : "(Disabled):BannerViewAd");

		if (mGMAHReference.enableBannerViewAd) {

			EditorGUI.indentLevel += 1;

			EditorGUILayout.Space ();

			EditorGUILayout.BeginVertical ();
			EditorGUILayout.PropertyField (
				serializedObject.FindProperty ("bannerAdPosition"));
			if (!mGMAHReference.enableDevelopmentBuild && mGMAHReference.enable_Android_Platform) {

				mGMAHReference.adUnitIdBannerViewAd_Android = EditorGUILayout.TextField (
					"adUnitId_Android",
					mGMAHReference.adUnitIdBannerViewAd_Android);
			}
			if(!mGMAHReference.enableDevelopmentBuild && mGMAHReference.enable_iOS_Platform){

				mGMAHReference.adUnitIdBannerViewAd_iOS = EditorGUILayout.TextField (
					"adUnitId_iOS",
					mGMAHReference.adUnitIdBannerViewAd_iOS);
			}
			EditorGUILayout.EndVertical ();

			EditorGUILayout.BeginHorizontal ();
			mGMAHReference.bannerViewAdFailedCallback = EditorGUILayout.Toggle (
				"OnBannerViewAdFailed",
				mGMAHReference.bannerViewAdFailedCallback);

			if (mGMAHReference.bannerViewAdFailedCallback) {

				EditorGUILayout.PropertyField (
					serializedObject.FindProperty ("OnBannerViewAdFailedEvent"));

			}
			EditorGUILayout.EndHorizontal ();


			EditorGUILayout.BeginHorizontal ();
			mGMAHReference.bannerViewAdClosedCallback = EditorGUILayout.Toggle (
				"OnBannerViewAdClosed",
				mGMAHReference.bannerViewAdClosedCallback);

			if (mGMAHReference.bannerViewAdClosedCallback) {

				EditorGUILayout.PropertyField (
					serializedObject.FindProperty ("OnBannerViewAdClosedEvent"));

			}
			EditorGUILayout.EndHorizontal ();

			EditorGUI.indentLevel -= 1;
		}
	}
}
