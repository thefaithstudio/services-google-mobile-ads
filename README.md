# Documentation
You can find the full **Documentation** on the following [link](https://bitbucket.org/PGS_ART/service-google-mobile-ads/src/b96f04bb06617b889e5eb061044cabf1f0f99044/Assets/_HelpBox/GoogleMobileAds/Documentation.docx?at=master&fileviewer=file-view-default)

# Editor Settings

- **Development Build:** If your application is not linked with the playstore & appstore, click on the enable 'Development Build'. By enabling the development build, the application will now use test appId/adUnitId given by the admob. You must put your device id on the 'DeviceID' section. This app will help you to get your device id [https://play.google.com/store/apps/details?id=com.evozi.deviceid](https://play.google.com/store/apps/details?id=com.evozi.deviceid). If the 'Development Build' is disabled, you have to add appId/adUnitId for different platforms (Android/iOS).

- **TestGUI:**'Test GUI' will allow you to test 'AdMob' on physical device by enabling 'OnGUI' interface. By enabling it, you will be able to test 'AdMob' on physical devices with the help of 'OnGUI&' interface.

- **Android & iOS (Platform Selection):** By enabling the platform, you have to put the appId/adUnitId for those specific platform while the 'Development Build' is disabled. If the 'Development Build' is enabled, the appId/adUnitId parameter would be disabled from the unity editor as it's going to be auto filled by the editor script with the test appId/adUnitId of **admob.**

- **Enabling & Disabling Ad-Services:** You could enable and disabled different ad services by simply expanding each services from the unity editor by clicking on the 'Expand' icon.

- **RewardBasedVideoAd:** Just put the **appId** for each platform (Android/iOS) and make sure to set call back for **OnRewardBasedVideoAdFinished** event. You can also set some extra event like **OnRewardBasedVideoAdFailed** & **OnRewardBasedVideoAdSkip** by simply enabling from the unity editor.

- **InterstitialAd:** Just put the **adUnitId** for each platform (Android/iOS). You can also set some extra event like **OnInterstitialAdFailed** & **OnInterstitialAdSkip** by simply enabling from the unity editor.

- **BannerViewAd:** Just put the **adUnitId** for each platform (Android/iOS). You can also set some extra event like **OnBannerViewAdFailed** & **OnBannerViewAdSkip** by simply enabling from the unity editor. You can also set the banner ad position from the dropdown list of 'Banner Ad Position' (By default, it has been set to Bottom).

# Custom Function

**Overview:** As the following script itself singleton, you can access to its public variable by simply accessing **GoogleMobileAdsHandler.Instance.YourFunction**.

**ShowAds():** Show either 'RewardBasedVideoAd' or 'InterstetialAd' based on which ad is ready to be display. Make sure the ad is enabled from the unity editor. It will also show 'RewardBasedVideoAd' over 'InterstetialAd' if the 'RewardBasedVideoAd' is ready to display as it gives more revenue than any other displayed ad.

- **RewardBasedVideoAd:**
  - **IsRewardBasedVideoAdReady():** return true. If the 'RewardBasedVideoAd' is enabled & ready.
  - **ShowRewardBasedVideoAd():** Show 'RewardBasedVideoAd' if ready. If not, it requests to load the ad and display when the ad is ready. If the attempt gets failed few times. It stops loading the ads and failed to show the ad.
  - **ShowRewardBasedVideoAd(UnityAction OnAdsFinished):** Show 'RewardBasedVideoAd' with custom call back of finishing ads (OnAdsFinished).
  - **ShowRewardBasedVideoAd(UnityAction OnAdsFinished, UnityAction OnAdsFailed):** Show 'RewardBasedVideoAd'with custom call back of finishing ads (OnAdsFinished) &amp; failed to show ads (OnAdsFailed).
  - **ShowRewardBasedVideoAd(UnityAction OnAdsFinished, UnityAction OnAdsFailed, UnityAction OnAdsSkipped):** Show 'RewardBasedVideoAd' with custom call back of finishing ads (OnAdsFinished), failed to show ads (OnAdsFailed) & skipping of current ad (OnAdSkipped).
- **InterstetialAd:**
  - **IsInterstetialAdReady():** return true. If the 'InterstetialAd' is enabled & ready.
  - **ShowInterstetialAd():** Show 'InterstetialAd' if ready. If not, it requests to load the ad and display when the ad is ready. If the attempt gets failed few times. It stops loading the ads and failed to show the ad.
  - **ShowInterstetialAd(UnityAction OnAdsFailed):** Show 'InterstetialAd' with custom call back of failing to show ads (OnAdsFailed).
  - **ShowInterstetialAd(UnityAction OnAdsFailed, UnityAction OnAdsClosed):** Show 'InterstetialAd' with custom call back of failing to show ads (OnAdsFailed) & when the ads is closed (OnAdsClosed).
- **BannerViewAd:**
  - **ShowBannerViewAd():** Show 'BannerViewAd' when the function is called.
  - **ShowBannerViewAd(UnityAction OnAdsFailed):** Show 'BannerViewAd' with custom call back of failing to show ads (OnAdsFailed).
  - **ShowBannerViewAd(UnityAction OnAdsFailed, UnityAction OnAdsClosed):** Show 'BannerViewAd' with custom call back of failing to show ads (OnAdsFailed) & when the ads is closed (OnAdsClosed).
  - **DisplayBannerViewAd():** Show the banner ad that has already been loaded or hided from the screen.
  - **HideBannerViewAd():** Hide the banner view ad which has been displaying on the screen.
  - **DestroyBannerViewAd():** Destroy the banner view ad from the screen. You can display the bannerAd by simply calling 'ShowBannerViewAd()' to load and show the ads.

